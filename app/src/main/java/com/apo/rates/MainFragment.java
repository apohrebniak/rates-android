package com.apo.rates;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apo.rates.adapter.BanksCursorAdapter;
import com.apo.rates.db.DbHelper;
//import com.apo.rates.model.BanksResponse;
import com.apo.rates.deserializer.ResponseDeserializer;
import com.apo.rates.modelnew.BanksResponse;
import com.apo.rates.rest.ApiClient;
import com.apo.rates.rest.ApiInterface;

import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Main fragment with main rate
 */
public class MainFragment extends Fragment {

    private static final String KEY_PREF_FAV_CURR = "pref_fav_curr";
    public static final String KEY_DATE = "pref_date";
    public static final String CURRENCY_DOLLAR = "USD";
    public static final String CURRENCY_EURO = "EUR";

    private static boolean FLAG_UPDATED = false;
    private static String FLAG_CURRENCY;
    private static int FLAG_SCREEN = 0;

    ListView banksList;
    ProgressBar progressBar;
    BanksCursorAdapter adapter = null;
    OnCurrencyChenged mCallback;
    SwipeRefreshLayout swipeLayout;
    TextView message;

    public interface OnCurrencyChenged{
        void changeActivityTitle(String currency);
        void changeActivitySubtitle(String newSubtitle);
    }


    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnCurrencyChenged) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnCurrencyChanged");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        progressBar = (ProgressBar)view.findViewById(R.id.main_fragment_progressBar);
        message = (TextView)view.findViewById(R.id.message_textview);
        banksList = (ListView)view.findViewById(R.id.banksList);
        swipeLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_layout);

        setupScreen();

        setupSwipeLayout();

        setupCurrency();

        setupAdapter();

        setupData();

        return view;
    }

    private void setupScreen(){
        if(FLAG_SCREEN != 0) {
            if (FLAG_SCREEN == 1)
                showMessage();
            if(FLAG_SCREEN == 2)
                showList();
        }
    }

    private void setupData(){
        if(FLAG_UPDATED)
            showUpdatedData(); //data was updated. activity is recreated after conf changes
        else{
            if(isNetworkConnected())
                updateData(); //data wasn't updated. network is available. update data
            else {
                Toast.makeText(getActivity(), getResources()
                        .getString(R.string.internet_unavailable), Toast.LENGTH_LONG).show();
                if (!DbHelper.getInstance(getActivity()).isEmpty())
                    showUpdatedData(); //network is not available, but db is not empty. show stored data
                else
                    showMessage(); //no ntework and no data is stored. show message
            }
        }
    }

    private void setupAdapter(){
        if(adapter != null)
            banksList.setAdapter(adapter);
        else {
            adapter = new BanksCursorAdapter(getActivity(), null, 0);
            banksList.setAdapter(adapter);
        }
    }

    private void setupCurrency(){
        if(FLAG_CURRENCY == null){
            String prefCurrency = PreferenceManager
                    .getDefaultSharedPreferences(getActivity())
                    .getString(KEY_PREF_FAV_CURR,CURRENCY_DOLLAR);
            if(prefCurrency.equals(CURRENCY_DOLLAR))
                FLAG_CURRENCY = CURRENCY_DOLLAR;
            else if(prefCurrency.equals(CURRENCY_EURO))
                FLAG_CURRENCY = CURRENCY_EURO;
        }

        mCallback.changeActivityTitle(FLAG_CURRENCY);
    }

    private void setupSwipeLayout(){
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isNetworkConnected())
                    updateData();
                else{
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.internet_unavailable),
                            Toast.LENGTH_SHORT).show();
                    swipeLayout.setRefreshing(false);
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_change){

            if(FLAG_CURRENCY.equals(CURRENCY_DOLLAR)){
                Log.d("OPTION SELECTED", "ACTION_CHANGE FROM DOLLAR");
                FLAG_CURRENCY = CURRENCY_EURO;
                showUpdatedData();
            }
            else{
                Log.d("OPTION SELECTED", "ACTION_CHANGE FROM EURO");
                FLAG_CURRENCY = CURRENCY_DOLLAR;
                showUpdatedData();
            }
            return true;
        }

        if(id == R.id.action_refresh){
            if(isNetworkConnected()) {
                swipeLayout.setRefreshing(true);
                updateData();
            }
            else
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.internet_unavailable),
                        Toast.LENGTH_SHORT).show();
        }

        if(id == R.id.action_settings){
            startActivity(new Intent(getActivity(),SettingsActivity.class));
        }
        return false;
    }

    private boolean isNetworkConnected(){
        ConnectivityManager manager = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(manager.getActiveNetworkInfo() == null)
            return false;
        if(!manager.getActiveNetworkInfo().isAvailable())
            return false;
        return true;
    }

    private void showMessage(){
        FLAG_SCREEN = 1;
        swipeLayout.setRefreshing(false);
        message.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        banksList.setVisibility(View.GONE);
        swipeLayout.setRefreshing(false);
    }

    private void showList(){
        FLAG_SCREEN = 2;
        message.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        banksList.setVisibility(View.VISIBLE);
        Log.d("HELLO", "IN SHOW  LIST");
    }

    private void updateData(){
        Log.d("HEllo", "THIS IS UPDATE DATA");
        buildCurrentDateString();
        Log.d("UPDATE DATA", ResponseDeserializer.CURRENT_DATE);
        makeApiRequest();
        Log.d("REQUEST -> ", "DONE");

    }

    private void updateToolbarDate(){
        Log.d("HELLO", "UPDATE TOOLBAR DATE");
        Date date = new Date();
        String timeString = android.text.format.DateFormat.getTimeFormat(getActivity()).format(date);

        StringBuilder builder = new StringBuilder();
        builder.append(getResources().getString(R.string.updatedToolbar))
                .append(" ")
                .append(timeString);
        String newDateString = builder.toString();

        SharedPreferences prefs = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(KEY_DATE, newDateString);
        editor.apply();
    }

    private void makeApiRequest(){

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class); //get instance

        Call<BanksResponse> call = apiService.getBanks(); // make request

        call.enqueue(new Callback<BanksResponse>() {
            @Override
            public void onResponse(Call<BanksResponse> call, Response<BanksResponse> response) {
                Log.d("RESPONSE ---->", response.body().getStatus());

                new PopulateDatabase().execute(response.body());//TODO: fix this

            }

            @Override
            public void onFailure(Call<BanksResponse> call, Throwable t) {
                Log.d("ERRRRRRRRR", "FEILURE!!!");
                t.printStackTrace();
            }
        });

    }

    private void showUpdatedData(){
        Cursor cursor = null;

            if(FLAG_CURRENCY.equals(CURRENCY_DOLLAR))
                cursor = DbHelper.getInstance(getActivity()).getAllDollarCursor();
            if(FLAG_CURRENCY.equals(CURRENCY_EURO))
                cursor = DbHelper.getInstance(getActivity()).getAllEuroCursor();

            adapter.changeCursor(cursor);
            showList();

        mCallback.changeActivityTitle(FLAG_CURRENCY);
        mCallback.changeActivitySubtitle(getActivity()
                .getPreferences(Context.MODE_PRIVATE).getString(KEY_DATE,""));

        Log.d("HELLO", "IN UPDATE DATA");
    }

    private void buildCurrentDateString(){
        int year, month, day;
        Calendar calendar = Calendar.getInstance();
        StringBuilder builder = new StringBuilder();

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);

        builder.append(year).append("-");
        if(month < 10)
            builder.append("0");
        builder.append(month).append("-");
        if(day < 10)
            builder.append("0");
        builder.append(day);

        ResponseDeserializer.CURRENT_DATE = builder.toString();

    }

    public class PopulateDatabase extends AsyncTask<BanksResponse, Void, Integer>{
        BanksResponse response;
        DbHelper helper;

        @Override
        protected Integer doInBackground(BanksResponse... params) {
            response = params[0];
            if(response != null && response.getDate() != null){
                helper = DbHelper.getInstance(getActivity().getApplicationContext());
                helper.populateDollarTable(response);
                helper.populateEuroTable(response);
                return Integer.valueOf(1);
            } else
                return Integer.valueOf(0);

        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if(result == 1){
                FLAG_UPDATED = true;
                swipeLayout.setRefreshing(false);
                Toast.makeText(getActivity().getApplicationContext(),
                        R.string.updated_toast, Toast.LENGTH_SHORT).show();
                updateToolbarDate();
                showUpdatedData();
            }
            if(result == 0){
                if(!DbHelper.getInstance(getActivity()).isEmpty()) {
                    showUpdatedData();
                }
                else
                    showMessage();
                }
            }
        }
    }





