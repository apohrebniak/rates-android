package com.apo.rates.modelnew;

public class BanksResponse {

    private String status;
    private Date date;

    public BanksResponse(String status, Date date){
        this.status = status;
        this.date = date;
    }

    public BanksResponse(){

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
