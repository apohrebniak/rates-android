package com.apo.rates.modelnew;


public class Currency {

    private Bank[] banks;

    public Currency(Bank[] banks) {
        this.banks = banks;
    }

    public Currency(){

    }

    public Bank[] getBanks() {
        return banks;
    }

    public void setBanks(Bank[] banks) {
        this.banks = banks;
    }
}
