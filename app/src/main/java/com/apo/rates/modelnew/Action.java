package com.apo.rates.modelnew;

public class Action {
    private String value;
    private float diff;

    public Action(String value, float diff) {
        this.value = value;
        this.diff = diff;
    }

    public Action(){

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public float getDiff() {
        return diff;
    }

    public void setDiff(float diff) {
        this.diff = diff;
    }
}
