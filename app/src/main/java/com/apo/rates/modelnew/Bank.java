package com.apo.rates.modelnew;

public class Bank {

    private Action buy;
    private Action sale;

    public Bank(Action buy, Action sale) {
        this.buy = buy;
        this.sale = sale;
    }

    public Bank(){

    }

    public Action getBuy() {
        return buy;
    }

    public void setBuy(Action buy) {
        this.buy = buy;
    }

    public Action getSale() {
        return sale;
    }

    public void setSale(Action sale) {
        this.sale = sale;
    }
}
