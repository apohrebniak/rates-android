package com.apo.rates.modelnew;

public class Date {

    private Currency dollar;
    private Currency euro;

    public Date(Currency dollar, Currency euro) {
        this.dollar = dollar;
        this.euro = euro;
    }

    public Date(){

    }

    public Currency getDollar() {
        return dollar;
    }

    public void setDollar(Currency dollar) {
        this.dollar = dollar;
    }

    public Currency getEuro() {
        return euro;
    }

    public void setEuro(Currency euro) {
        this.euro = euro;
    }
}
