package com.apo.rates;

import android.content.SharedPreferences;
import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(getSupportFragmentManager().findFragmentById(R.id.preference_fragment_container) == null){
            getFragmentManager().beginTransaction()
                    .replace(R.id.preference_fragment_container, new PrefFragment()).commit();
        }
    }
}
