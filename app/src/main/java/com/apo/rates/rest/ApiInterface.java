package com.apo.rates.rest;

//import com.apo.rates.model.BanksResponse;
import com.apo.rates.modelnew.BanksResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by druny_000 on 28.07.2016.
 */
public interface ApiInterface {
    @Headers({
            "X-Mashape-Key: XoFY9YK31YmshXqEI0Y3HpzhRAjUp1VKST8jsnkwinauc7hJSM",
            "Accept: application/json"
    })
    @GET("/v1/rates/banks")
    Call<BanksResponse> getBanks();
}
