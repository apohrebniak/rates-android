package com.apo.rates.rest;

import com.apo.rates.deserializer.ActionDeserializer;
import com.apo.rates.deserializer.BankDeserializer;
import com.apo.rates.deserializer.CurrencyDeserializer;
import com.apo.rates.deserializer.DateDeserializer;
import com.apo.rates.deserializer.ResponseDeserializer;
import com.apo.rates.modelnew.Action;
import com.apo.rates.modelnew.Bank;
import com.apo.rates.modelnew.BanksResponse;
import com.apo.rates.modelnew.Currency;
import com.apo.rates.modelnew.Date;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static final String BASE_URL = "https://hryvna-today.p.mashape.com";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            Gson gson = configureGson();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    private static Gson configureGson(){
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(BanksResponse.class, new ResponseDeserializer());
        builder.registerTypeAdapter(Date.class, new DateDeserializer());
        builder.registerTypeAdapter(Currency.class, new CurrencyDeserializer());
        builder.registerTypeAdapter(Bank.class, new BankDeserializer());
        builder.registerTypeAdapter(Action.class, new ActionDeserializer());

        return builder.create();
    }
}
