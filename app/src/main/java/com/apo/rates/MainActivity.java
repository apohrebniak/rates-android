package com.apo.rates;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;

import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements MainFragment.OnCurrencyChenged {

    Toolbar toolbar;
    FragmentManager manager;
    Fragment mainFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupToolbar();

        manager = getSupportFragmentManager();
        mainFragment = manager.findFragmentById(R.id.fragment_container);
        if(mainFragment == null){
            mainFragment = new MainFragment();
            manager.beginTransaction().replace(R.id.fragment_container, mainFragment,"TAG_MAIN")
                    .commit();
        }

    }

    private void setupToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void changeActivityTitle(String currency) {
        if(currency.equals(MainFragment.CURRENCY_DOLLAR))
            getSupportActionBar().setTitle(R.string.title_usd);
        if(currency.equals(MainFragment.CURRENCY_EURO))
            getSupportActionBar().setTitle(R.string.title_eur);
        Log.d("CHANGE TITLE", currency);
    }

    @Override
    public void changeActivitySubtitle(String newSubtitle) {
        getSupportActionBar().setSubtitle(newSubtitle);
    }
}
