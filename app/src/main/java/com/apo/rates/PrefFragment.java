package com.apo.rates;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;


public class PrefFragment extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener{

    private static final String KEY_PREF_FAV_CURR = "pref_fav_curr";

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager()
                .getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        getPreferenceManager()
                .getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals(KEY_PREF_FAV_CURR)){
            Preference favCurrPref = findPreference(key);
            // Set summary to be the user-description for the selected value
            favCurrPref.setSummary(sharedPreferences.getString(key,""));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        findPreference(KEY_PREF_FAV_CURR)
                .setSummary(PreferenceManager.getDefaultSharedPreferences(getActivity())
                        .getString(KEY_PREF_FAV_CURR,""));
    }
}
