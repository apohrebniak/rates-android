package com.apo.rates.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apo.rates.R;
import com.apo.rates.db.DbHelper;

import java.util.Locale;

public class BanksCursorAdapter extends CursorAdapter {

    LayoutInflater inflater;
    String[] banks;
    float askValue;
    float bidValue;
    float askDiffValue;
    float bidDiffValue;

    int redColor;
    int greenColor;
    int grayColor;

    public BanksCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        inflater = LayoutInflater.from(context);
        banks = context.getResources().getStringArray(R.array.banks_array);
        redColor = context.getResources().getColor(R.color.colorRedDiff);
        greenColor = context.getResources().getColor(R.color.colorGreenDiff);
        grayColor = context.getResources().getColor(R.color.colorBankDiff);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = inflater.inflate(R.layout.item_banks_list, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView title = (TextView) view.findViewById(R.id.bank_title_textview);
        TextView ask = (TextView) view.findViewById(R.id.bank_ask_textview);
        TextView bid = (TextView) view.findViewById(R.id.bank_bid_textview);
        TextView ask_diff = (TextView) view.findViewById(R.id.bank_ask_diff);
        TextView bid_diff = (TextView) view.findViewById(R.id.bank_bid_diff);

        if(cursor != null){

            title.setText(banks[cursor.getPosition()]);

            askValue = Float.valueOf(cursor.getString(cursor.getColumnIndex(DbHelper.KEY_BUY)));
            ask.setText(String.format(Locale.getDefault(),"%2.2f", askValue));

            bidValue = Float.valueOf(cursor.getString(cursor.getColumnIndex(DbHelper.KEY_SALE)));
            bid.setText(String.format(Locale.getDefault(), "%2.2f", bidValue));

            askDiffValue = Float.valueOf(cursor.getString(cursor.getColumnIndex(DbHelper.KEY_BUY_DIFF)));
            if(askDiffValue > 0){
                ask_diff.setTextColor(redColor);
                ask_diff.setText(String.format(Locale.getDefault(), "+%2.2f", Math.abs(askDiffValue)));
            } else if(askDiffValue < 0){
                ask_diff.setTextColor(greenColor);
                ask_diff.setText(String.format(Locale.getDefault(), "-%2.2f", Math.abs(askDiffValue)));
            } else if(askDiffValue == 0){
                ask_diff.setTextColor(grayColor);
                ask_diff.setText(String.format(Locale.getDefault(), "%2.2f", Math.abs(askDiffValue)));
            }



            bidDiffValue = Float.valueOf(cursor.getString(cursor.getColumnIndex(DbHelper.KEY_SALE_DIFF)));
            bid_diff.setText(String.format(Locale.getDefault(), "%2.2f", bidDiffValue));
            if(bidDiffValue > 0){
                bid_diff.setTextColor(redColor);
                bid_diff.setText(String.format(Locale.getDefault(), "+%2.2f", Math.abs(bidDiffValue)));
            } else if(bidDiffValue < 0){
                bid_diff.setTextColor(greenColor);
                bid_diff.setText(String.format(Locale.getDefault(), "-%2.2f", Math.abs(bidDiffValue)));
            } else if (bidDiffValue == 0){
                bid_diff.setTextColor(grayColor);
                bid_diff.setText(String.format(Locale.getDefault(), "%2.2f", Math.abs(bidDiffValue)));
            }

        }

    }

}


