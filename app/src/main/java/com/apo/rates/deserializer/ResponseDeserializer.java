package com.apo.rates.deserializer;


import com.apo.rates.modelnew.BanksResponse;
import com.apo.rates.modelnew.Date;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ResponseDeserializer implements JsonDeserializer<BanksResponse> {

    public static String CURRENT_DATE;

    @Override
    public BanksResponse deserialize(JsonElement json, Type typeOfT,
                                     JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        final JsonObject jsonData = jsonObject.getAsJsonObject("data");

        String status = jsonObject.get("status").getAsString();
        Date date = context.deserialize(jsonData.get(CURRENT_DATE), Date.class);

        BanksResponse response = new BanksResponse();
        response.setStatus(status);
        response.setDate(date);
        return response;
    }
}
