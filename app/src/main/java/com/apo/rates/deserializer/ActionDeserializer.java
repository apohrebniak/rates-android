package com.apo.rates.deserializer;

import com.apo.rates.modelnew.Action;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ActionDeserializer implements JsonDeserializer<Action> {
    @Override
    public Action deserialize(JsonElement json, Type typeOfT,
                              JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();

        String value = jsonObject.get("value").getAsString();
        float diff = jsonObject.get("diff").getAsFloat();

        Action action = new Action();
        action.setValue(value);
        action.setDiff(diff);
        return action;
    }
}
