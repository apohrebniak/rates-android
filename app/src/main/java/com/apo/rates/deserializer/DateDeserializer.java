package com.apo.rates.deserializer;

import com.apo.rates.modelnew.Currency;
import com.apo.rates.modelnew.Date;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class DateDeserializer implements JsonDeserializer<Date>{
    @Override
    public Date deserialize(JsonElement json, Type typeOfT,
                            JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        Currency dollar = context.deserialize(jsonObject.get("840"), Currency.class);
        Currency euro = context.deserialize(jsonObject.get("978"), Currency.class);

        Date date = new Date();
        date.setDollar(dollar);
        date.setEuro(euro);
        return date;
    }
}
