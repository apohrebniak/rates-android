package com.apo.rates.deserializer;

import com.apo.rates.modelnew.Bank;
import com.apo.rates.modelnew.Currency;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class CurrencyDeserializer implements JsonDeserializer<Currency>{
    @Override
    public Currency deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();

        Bank[] banks = new Bank[16];

        for(int i = 0; i < 16; i++ ){
            if(i >= 14){
                banks[i] = context.deserialize(jsonObject.get(String.valueOf(i+2)), Bank.class);
            }
            else
                banks[i] = context.deserialize(jsonObject.get(String.valueOf(i+1)), Bank.class);
        }

        Currency currency = new Currency();
        currency.setBanks(banks);
        return currency;
    }
}
