package com.apo.rates.deserializer;


import com.apo.rates.modelnew.Action;
import com.apo.rates.modelnew.Bank;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class BankDeserializer implements JsonDeserializer<Bank>{
    @Override
    public Bank deserialize(JsonElement json, Type typeOfT,
                            JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();

        Action buy = context.deserialize(jsonObject.get("buy"), Action.class);
        Action sale = context.deserialize(jsonObject.get("sale"), Action.class);

        Bank bank = new Bank();
        bank.setBuy(buy);
        bank.setSale(sale);

        return bank;
    }
}
