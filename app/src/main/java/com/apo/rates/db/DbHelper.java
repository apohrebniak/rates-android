package com.apo.rates.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.apo.rates.modelnew.Bank;
import com.apo.rates.modelnew.BanksResponse;
import com.apo.rates.modelnew.Currency;

//TODO: helper
public class DbHelper extends SQLiteOpenHelper {

    private final static String DATABASE_NAME = "currencies";

    private final static String TABLE_DOLLAR = "dollar";
    private final static String TABLE_EURO = "euro";

    public final static String KEY_ID = "_id";
    public final static String KEY_SALE = "sale";
    public final static String KEY_SALE_DIFF = "sale_diff";
    public final static String KEY_BUY = "buy";
    public final static String KEY_BUY_DIFF = "buy_diff";

    public static DbHelper helper = null;

    public DbHelper(Context context){
        super(context, DATABASE_NAME, null, 5);
    }

    public static DbHelper getInstance(Context context){
        if(helper == null)
            return new DbHelper(context);
        else
            return helper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_DOLLAR + "( " + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_SALE + " TEXT, " + KEY_SALE_DIFF + " REAL, " + KEY_BUY + " TEXT, "
                + KEY_BUY_DIFF + " REAL)");

        db.execSQL("CREATE TABLE " + TABLE_EURO + "(" + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_SALE + " TEXT, " + KEY_SALE_DIFF + "  REAL, " + KEY_BUY + " TEXT, "
                + KEY_BUY_DIFF + " REAL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DOLLAR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EURO);

        onCreate(db);
    }

    public void populateDollarTable(BanksResponse response){
        Currency dollar;
        Bank[] banks;
        ContentValues values = new ContentValues();
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_DOLLAR);

        dollar = response.getDate().getDollar();
        banks = dollar.getBanks();

        for (Bank bank : banks) {
            values.put(KEY_SALE, bank.getSale().getValue());
            values.put(KEY_SALE_DIFF, bank.getSale().getDiff());
            values.put(KEY_BUY, bank.getBuy().getValue());
            values.put(KEY_BUY_DIFF, bank.getBuy().getDiff());
            db.insert(TABLE_DOLLAR,null,values);
            values.clear();
        }

        db.close();
    }

    public void populateEuroTable(BanksResponse response){
        Currency euro;
        Bank[] banks;
        ContentValues values = new ContentValues();
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_EURO);

        euro = response.getDate().getEuro();
        banks = euro.getBanks();

        for (Bank bank : banks) {
            values.put(KEY_SALE, bank.getSale().getValue());
            values.put(KEY_SALE_DIFF, bank.getSale().getDiff());
            values.put(KEY_BUY, bank.getBuy().getValue());
            values.put(KEY_BUY_DIFF, bank.getBuy().getDiff());
            db.insert(TABLE_EURO,null,values);
            values.clear();
        }

        db.close();
    }


    public Cursor getAllDollarCursor(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_DOLLAR,null);
        return cursor;
    }

    public Cursor getAllEuroCursor(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_EURO,null);
        return cursor;
    }

    public boolean isEmpty(){
        boolean isEmpty = false;
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT COUNT(*) FROM "+TABLE_DOLLAR, null);
        if(c != null && c.moveToFirst()){
            if(c.getInt(0) == 0) return true;
        }
        c = db.rawQuery("SELECT COUNT(*) FROM "+TABLE_EURO, null);
        if(c != null && c.moveToFirst()){
            if(c.getInt(0) == 0) return true;
        }
        return false;
    }
}

